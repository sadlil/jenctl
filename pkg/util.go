package pkg

import (
	"encoding/base64"
	"strings"
)

const (
	Encode = "ENCODE"
	Decode = "DECODE"
)

func convert(data, t string) (ret string) {
	if t == Encode {
		ret = base64.StdEncoding.EncodeToString([]byte(data))
	} else {
		d, _ := base64.StdEncoding.DecodeString(data)
		ret = string(d)
	}
	return ret
}

func getUserNamePass(data string) (string, string) {
	return data[0 : strings.Index(data, ":")], data[strings.Index(data, ":") + 1 :]
}

