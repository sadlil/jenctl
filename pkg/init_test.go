package pkg

import (
	"testing"
	"fmt"
	"gopkg.in/go-playground/assert.v1"
	"github.com/spf13/viper"
)

func TestReadConfig(t *testing.T) {
	viperInit()
	a := readConfig()
	fmt.Println(a, a.JenkinsUrl)
	assert.NotEqual(t, a.JenkinsUrl, nil)
}

func TestReadWrite(t *testing.T) {
	viperInit()
	writeConfig(&jenkinsConfig{
		JenkinsUrl: "newTestUrl",
		UserName: "sadlil",
		Password: "nopassword",
	})
	assert.Equal(t, viper.Get("jenkinsUrl"), "newTestUrl")
}