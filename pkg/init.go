package pkg

import (
	"github.com/spf13/viper"
	"encoding/json"
	"io/ioutil"
	"os"
)

type jenkinsConfig struct {
	JenkinsUrl string
	UserName   string
	Password   string
}

var config *jenkinsConfig

func GetConfig() *jenkinsConfig {
	if config != nil {
		return config
	}
	config = readConfig()
	return config
}

func SetConfig(url, username, pass string) {
	if config != nil {
		config.JenkinsUrl = url
		config.UserName = username
		config.Password = pass
		writeConfig(config)
		return
	}
	config = &jenkinsConfig{
		JenkinsUrl: url,
		UserName: username,
		Password: pass,
	}
	writeConfig(config)
}

func InitJenCtl() {
	viperInit()
	config = GetConfig()
}

func viperInit() error {
	viper.SetConfigName(".jenconf")
	viper.AddConfigPath("$HOME/")
	viper.SetConfigType("json")
	viper.SetDefault("jenkinsUrl", "")
	viper.SetDefault("auth", "")
	err := viper.ReadInConfig()
	return err
}


func readConfig() *jenkinsConfig {
	host := viper.GetString("jenkinsUrl")
	auth := viper.GetString("auth")

	if host == "" {
		return nil
	}

	d := convert(auth, Decode)
	var username, password string
	if d != "" {
		username, password = getUserNamePass(d)
	}
	return &jenkinsConfig{
		JenkinsUrl: host,
		UserName: username,
		Password: password,
	}
}

func writeConfig(config *jenkinsConfig) {
	authString := convert(config.UserName + ":" + config.Password, Encode)
	viper.Set("jenkinsUrl", config.JenkinsUrl)
	viper.Set("auth", authString)
	fileMap := make(map[string]string)
	fileMap["jenkinsUrl"] = config.JenkinsUrl
	fileMap["auth"] = authString

	b, _ := json.Marshal(fileMap)
	ioutil.WriteFile(os.Getenv("HOME") + "/.jenconf.json", b, os.ModePerm)
}