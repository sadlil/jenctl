package main

import (
	"github.com/sadlil/jenctl/cmd"
	"os"
)

func main() {
	if cmd.Run() != nil {
		os.Exit(1)
	}
}
