package pkg

import (
	"testing"
	"gopkg.in/go-playground/assert.v1"
)

func TestConvert(t *testing.T) {
	stringData := "Hello:Test"
	e := convert(stringData, Encode)
	d := convert(e, Decode)
	assert.Equal(t, stringData, d)
}

func TestUserNamePass(t *testing.T) {
	a, b := getUserNamePass("Sadlil:Rhythm")
	assert.Equal(t, a, "Sadlil")
	assert.Equal(t, b, "Rhythm")
}