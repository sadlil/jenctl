package cmd

import (
	"github.com/sadlil/jenctl/pkg"
	"github.com/spf13/cobra"
)

func Run() error {
	cobra.OnInitialize(pkg.InitJenCtl)
	rootCmd := &cobra.Command{
		Use:   "jenctl",
		Short: "jenkins cli v0.1",
		Long:  "jenkins cli for managing jenkins",
		Run:   rootCmdRun,
	}
	rootCmd.AddCommand(NewLoginCommand())
	return rootCmd.Execute()
}

func rootCmdRun(cmd *cobra.Command, args []string) {
	cmd.Help()
}
