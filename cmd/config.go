package cmd
import (
	"github.com/spf13/cobra"
	"fmt"
	"github.com/sadlil/jenctl/pkg"
)

func NewLoginCommand() *cobra.Command {
	loginCmd := &cobra.Command{
		Use: "login",
		Short: "login to jenkins",
		Long: "use jenctl login to add an jenkins with or without basic auth",

		Run: runLogin,

	}
	return loginCmd
}

func runLogin(cmd *cobra.Command, args []string) {
	var url, username, pass string
	fmt.Println("Enter Jenkins Url with Port (Ex: test.jenkins.com:8090):")
	fmt.Scanln(&url)
	fmt.Println("Enter Jenkins Username, Leave it blank for No Authentication")
	fmt.Scanln(&username)
	if username != "" {
		fmt.Println("Enter Jenkins Password")
		fmt.Scanln(&pass)
	} else {
		pass = ""
	}
	pkg.SetConfig(url, username, pass)
}